# WordPress Installer / Launcher

Generates a WordPress installation, including the defaults one typically spends an hour enabling and fixing when creating a new WordPress installation. Also includes a "pre-launch cleaner" which removes development-related items, theme testing data, and similar.

## Requirements
* [WP-CLI](http://wp-cli.org/)

### Installer
* **Use:** ```bash wordpress.sh <db-name> <site-url> <admin-email> <install-dir>```
* **Actual example:** ```bash ~/Desktop/wordpress.sh scaffold http://scaffold.dev katew@jkdesign.com ~/Sites/scaffold```

### Cleaner
* **Use:** ```bash wordpress-prelaunch.sh <install-dir>```
* **Actual example:** ```bash ~/Desktop/wordpress-prelaunch.sh ~/Sites/scaffold/www```
* Note: the installer puts WordPress in a www/ directory. That's where the prelaunch script needs to run, so when running the prelaunch script, you have to append www to your install directory. At least, until someone smarter than me figures out something better.

## What it does
### wordpress.sh -- WordPress Installation
* Generates a database prefix
* Generates a password for the admin user "jkdeveloper"
* Writes the install URL, admin username, and password to a local file on your desktop way outside of the WordPress installation
* Creates a blank robots.txt file with a global Allow directive (to prevent WordPress's default robots.txt from being used)
* Copies the [WordPress plugin boilerplate](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate) into a created mu-plugins folder
* Installs a copy of [Underscores/_s](http://underscores.me) with Sass and activates it
* Deletes default twenty* WordPress themes
* Sets your permalink structure to %postname% and flushes rewrites
* Deletes the starter WordPress gunk: Hello Dolly, Hello World posts, pages
* Creates pages for homepage, blog, about, contact, privacy policy, and sitemap
* Defines some constants in wp-config.php:
	* Limits revisions to 10
	* Sets autosave interval to 300ms
	* Sets DISALLOW_FILE_EDIT to disable file edits in wp-admin
	* Disables automatic WordPress updates
	* Sets WP_MEMORY_LIMIT to 96M
	* Sets WP_DEBUG to true
	* Sets WP_DEBUG_LOG to true to log errors
	* Sets WP_DEBUG_DISPLAY to true to show errors on the front end
* Installs but **does not activate**:
	* [WordFence Security](https://wordpress.org/plugins/wordfence/)
	* [All in One SEO Pack](https://wordpress.org/plugins/all-in-one-seo-pack/)
	* [Updraft Plus](https://wordpress.org/plugins/updraftplus/)
	* [Imsanity](https://wordpress.org/plugins/imsanity/)
	* [EWWW Image Optimizer](https://wordpress.org/plugins/ewww-image-optimizer/)
	* [Better WordPress Minify (BWP Minify)](https://wordpress.org/plugins/bwp-minify/)
	* [Fastest Cache](https://wordpress.org/plugins/wp-fastest-cache/)
	* [WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/)
* Installs **and activates** some QA, development, and testing plugins and data: 
	* [Monster Widget](https://wordpress.org/plugins/monster-widget/) (plugin)
	* [RTL Tester](https://wordpress.org/plugins/rtl-tester/) (plugin)
	* [P3 Profiler](https://wordpress.org/plugins/p3-profiler/) (plugin)
	* [Broken Link Checker](https://wordpress.org/plugins/broken-link-checker/) (plugin)
	* [User Switching](https://wordpress.org/plugins/user-switching/) (plugin)
	* [User Role Editor](https://wordpress.org/plugins/user-role-editor/) (plugin)
	* [Query Monitor](https://wordpress.org/plugins/query-monitor/) (plugin)
	* [WP Max Submit Protect](https://wordpress.org/plugins/wp-max-submit-protect/) (plugin)
	* [Official WordPress theme test data](https://wpcom-themes.svn.automattic.com/demo/theme-unit-test-data.xml)
* Creates mu-plugin **wordpress-cleaner.php**:
	* Disables emojis
	* Strips script and style versions
	* Strips WordPress version
	* Strips admin bar from front end
	* Removes RSD and WLW (blog client interfaces) from The Generator
* Create mu-plugin **wordpress-admin-cleaner.php**:
	* Removes WordPress logo from login screen
	* Replaces "Howdy" with "Hello" in the upper right hand corner
	* Removes the WordPress icon and links in the upper left hand corner
	* Removes version number and "Thank you for creating with WordPress" from wp-admin footer
	* Removes default WordPress dashboard widgets
	* Creates a custom dashboard widget
	* "Removes" the ability for "clientadmin" role level users to upload ZIP files as plugin. **NOTE**: It doesn't actually remove the ability. It just uses CSS to disable the uploader. A technically-savvy client with the ability to use devtools will be able to bypass this.
	* Removes menu pages from the wp-admin dashboard for the "clientadmin" role -- leaves behind only posts, media, pages, comments, users, and the dashboard index.
* Users and Permissions:
	* Creates one **special role** -- Client Admin or clientadmin -- for client-specific changes and roles (to be added with User Role Editor plugin)
	* Creates one user per *default* role -- Subscriber, Contributor, Author, Editor -- for testing roles and permissions:
		* With fake @site.dev e-mail addresses. 
		* Each has an insecure password of **apple**
		* **Must be deleted** prior to site launch. The prelaunch script will delete these users for you automatically, but if you don't use the prelaunch script, you need to remember to delete these items.

## wordpress-prelaunch.sh -- WordPress Clean-Up
* Sets the [WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/) option to "dashboard" -- i.e., it hides wp-login.php for added security. Logins go to "dashboard" now rather than wp-login.php or wp-admin.
* Imports WordFence Security settings -- just a few things different from the defaults.
* Removes all of the theme tester XML data that was imported (posts, pages, tags, categories, menus, comments, attachments) 
* Removes the three default role testing accounts
* Removes the QA, testing, and development plugins
* Removes the development-related constants from wp-config.php
* It **does** leave behind:
	* Your "clientadmin" user with a new, regenerated password (does not write / output anywhere though, just makes sure an insecure password does not make it to the live site)
	* Your six created pages (home, blog, about, contact, privacy policy, sitemap)
	* Your active theme and files (of course!)

## Localhost vs 127.0.0.1 issues (Homestead / Vagrant)
* After the initial install, it won't let you do anything MySQL unless you're in vagrant via SSH.
* It hangs when I try to run when connected to vagrant via SSH. Just doesn't do anything and I have to CTRL+C to exit. It works to install if you're working locally.

Problem is there are things that need MySQL in the initial installation -- e.g., setting the front page to be the front page, the timezone, sidebars, etc. For right now I have a "workaround" so you can run the install script locally and still have it get all the MySQL stuff: it initially takes 127.0.0.1 as the database host, then as the very last command in the install script uses ```sed``` to replace the database host in wp-config.php with localhost. But after that point anything that deals with MySQL (e.g., ```wp options list```) fails unless you're in Vagrant via ssh.