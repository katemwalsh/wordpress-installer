#!/bin/bash
# WordPress automatic clean-up with WP-CLI

# usage prompt =================================================
if [ $# -lt 1 ]; then
	echo "usage: <install-dir>"
	exit 1
fi

INSTALLDIR=$1
cd $1

# confirm prompt =================================================
read -p "This script is intended to be used on a CONTENT-BARE site with NOTHING on it except theme customizations and data that was installed by the accompanying WordPress installation script. If you have already added content to your site you wish to keep, this script WILL PERMANENTLY DELETE YOUR CONTENT. ARE YOU SURE YOU WANT TO DO THIS? " -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	# localhost/127.0.0.1 fix
	sed -i "" -- "s/define('DB_HOST', 'localhost:33060');/define('DB_HOST', '127.0.0.1:33060');/g" wp-config.php

	# delete all menus =========================================

	wp menu delete $(wp menu list) --quiet

	# delete all pages =========================================

	wp post delete $(wp post list --post_type=page --post__not_in=2000,2001,2002,2003,2004,2005 --format=ids) --quiet --force --defer-term-counting

	# delete all posts =========================================

	wp post delete $(wp post list --post_type="post" --format=ids) --quiet --force --defer-term-counting

	# delete testing users =====================================

	wp user delete subscriber@site.dev --reassign="1" --quiet
	wp user delete contributor@site.dev --reassign="1" --quiet
	wp user delete author@site.dev --reassign="1" --quiet
	wp user delete editor@site.dev --reassign="1" --quiet


	# delete taxonomy terms ====================================
	wp term list post_tag --field=term_id | xargs wp term delete post_tag --quiet
	wp term list category --field=term_id | xargs wp term delete category --quiet


	# delete all media posts ===================================
	wp post delete $(wp post list --post_type='attachment' --format=ids)


	# delete all comments ======================================
	wp comment delete $(wp comment list --format=ids) --force


	# remove QA/test plugins ===================================
	INSTANT_ACTIVATE="monster-widget rtl-tester p3-profiler broken-link-checker user-switching wordpress-importer user-role-editor"

	wp plugin deactivate ${INSTANT_ACTIVATE} --quiet
	wp plugin delete ${INSTANT_ACTIVATE} --quiet


	# activate other plugins ===================================
	INACTIVE="imsanity ewww-image-optimizer all-in-one-seo-pack updraftplus wordfence bwp-minify fastest-cache wps-hide-login"
	wp plugin activate ${INACTIVE} --quiet


	# use "dashboard" instead of wp-login
	wp option update whl_page dashboard


	# change the "clientadmin" user password ===================
	GENPASS=(`cat /dev/random | LC_CTYPE=C tr -dc "[:alnum:]" | head -c 30`)
	wp user update client@site.dev --user_pass=${GENPASS}

	# add wordfence stuff ======================================
	echo "add_action('init','jkd__import_wordfence');
	function jkd__import_wordfence() {
	  try {
	      wordfence::importSettings('668475f7038403f22b93b44b72e5717e7f70c5ad896c1816c69e843a9d229170f268f826616489c54a1fa8faceaac1859fa21cdb736cc8ea5921aaf8a4bb3c5e');
	  } catch(Exception \$e){ echo \$e->getMessage(); }
	}


	add_action('init','jkd__import_aioseop');
	  function jkd__import_aioseop() {
	    try {
	        All_in_One_SEO_Pack_Importer_Exporter::do_importer_exporter('settings_aioseop.ini');
	    } catch(Exception \$e){ echo \$e->getMessage(); }
	  }" >> wp-content/mu-plugins/wordpress-cleaner.php

	# remove dev constants =====================================
	sed -i "" -- "/define( 'WP_DEBUG', true );/d" wp-config.php
	sed -i "" -- "/define( 'WP_DEBUG_LOG', true );/d" wp-config.php
	sed -i "" -- "/define( 'WP_DEBUG_DISPLAY', false );/d" wp-config.php


	# localhost/127.0.0.1 fix ==================================
	sed -i "" -- "s/define('DB_HOST', '127.0.0.1:33060');/define('DB_HOST', 'localhost:33060');/g" wp-config.php
fi