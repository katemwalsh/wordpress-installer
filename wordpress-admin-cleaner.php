<?php

/**
 * @wordpress-cleaner
 * Plugin Name:       WordPress Admin Cleaner
 * Plugin URI:        http://google.com
 * Description:       Provides some essential admin-end clean-up of WordPress.
 * Version:           1.0.0
 * Author:            JKD
 * Author URI:        http://jkdesign.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-cleaner
 */

# wp-admin menu pages disable ==================================
# disables everything in the wp-admin menu EXCEPT for what we
# exclude for the clientadmin role
# http://wordpress.stackexchange.com/a/160523

if ( ! function_exists( 'jkd__strip_admin_menu' ) ) {

  add_action( 'admin_menu', 'jkd__strip_admin_menu', 999 );

  function jkd__strip_admin_menu() {
    if ( current_user_can( 'clientadmin' ) ) {
      // Remove unnecessary menus
      // You will probably have to update this list for additional plugins, CPTs, etc.
      $menus_to_stay = array(
          // posts
          'edit.php',
          // pages
          'edit.php?post_type=page',
          //media
          'upload.php',
          // comments
          'edit-comments.php',
          // Dashboard
          'index.php',
          // Users
          'users.php'
      );      
      foreach ($GLOBALS['menu'] as $key => $value) {          
          if (!in_array($value[2], $menus_to_stay)) remove_menu_page($value[2]);
      }
    }
  }

}


# remove ability to upload ZIP plugins for clientadmin role
if ( ! function_exists( 'jkd__fakeblock_zips' ) ) {

  add_action('admin_head', 'jkd__fakeblock_zips');

  function jkd__fakeblock_zips() {
    if ( current_user_can( 'clientadmin' ) ) {
      $fakeblock = '/wp-admin/plugin-install.php?tab=upload';
      $url = $_SERVER[REQUEST_URI];
      if ( $url === $fakeblock ) {
          echo "<style>
          p.install-help { display:none; }
          .upload-plugin .wp-upload-form { max-width:500px; }
          form.wp-upload-form input,
          #install-plugin-submit
          { display: none; }
          form.wp-upload-form:before {
              content: 'Sorry, you cannot upload .ZIP plugin archive files due to potential security risks. Please browse the official WordPress plugin respository to find plugins for installation.';
              color: red;
              font-size: 22px;
              line-height: 1.5;
          }
          </style>";
      }
    }
  }

}

# remove wordpress logo from wp-login.php
if ( ! function_exists( 'jkd__my_login_logo' ) ) {

  add_action( 'login_enqueue_scripts', 'jkd__my_login_logo' );

  function jkd__my_login_logo() { ?>
      <style type="text/css">
          .login h1 a {
              background-image: none;
              display: none;
              padding-bottom: 30px;
          }
      </style>
  <?php }

}

# good-bye dashboard widgets! ==================================

if ( ! function_exists( 'jkd__remove_dashboard_meta' ) ) {

  add_action( 'admin_init', 'jkd__remove_dashboard_meta' );

  function jkd__remove_dashboard_meta() {

    # standard WordPress metaboxes =============================
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );

    # custom WordPress metaboxes ===============================
    # Broken Link Checker plugin
    # remove_meta_box( 'blc_dashboard_widget', 'dashboard', 'normal' );

    # themefusion news for Avada
    # remove_meta_box( 'themefusion_news', 'dashboard', 'normal' );

    # rocketgenius widget for Gravity Forms
    # remove_meta_box( 'rg_forms_dashboard', 'dashboard', 'side' );
  }

}

# remove WP welcome widget =====================================
remove_action( 'welcome_panel', 'wp_welcome_panel' );


# custom welcome widget -- needs edits =========================
/**
 *  Adds hidden content to admin_footer, then shows with jQuery, and inserts after welcome panel
 *
 *  @author Ren Ventura <EngageWP.com>
 *  @see http://www.engagewp.com/how-to-create-full-width-dashboard-widget-wordpress
 */
if ( ! function_exists( 'jkd__custom_dashboard_widget' ) ) {

  add_action( 'admin_footer', 'jkd__custom_dashboard_widget' );

  function jkd__custom_dashboard_widget() {
    // Bail if not viewing the main dashboard page
    if ( get_current_screen()->base !== 'dashboard' ) {
      return;
    }
    $homeurl = get_bloginfo('url') . '/';
    ?>

    <div id="custom-id" class="welcome-panel" style="display: none;">
      <div class="welcome-panel-content">
        <h2>Welcome to <?php echo get_bloginfo('name'); ?>!</h2>
        <p class="about-description">Your site is running on <b>WordPress</b>.</p>
        <div class="welcome-panel-column-container">
          <div class="welcome-panel-column">
            <h3>Get Started</h3>
            <ul>
              <li><a href="<?php echo $homeurl; ?>wp-admin/profile.php" class="welcome-icon welcome-edit-page">Edit your Profile and Settings</a></li>
              <li><a href="<?php echo $homeurl; ?>wp-admin/post-new.php" class="welcome-icon welcome-write-blog">Add a blog post</a></li>
              <li><a href="<?php echo $homeurl; ?>" class="welcome-icon welcome-view-site">View your site</a></li>
            </ul>
          </div>
          <div class="welcome-panel-column">
            <h3>WordPress Help and Information</h3>
            <ul>
              <li><a target="_blank" href="https://codex.wordpress.org/" class="welcome-icon welcome-learn-more">WordPress: The Codex</a></li>
              <li><a target="_blank" href="https://wordpress.org/support/" class="welcome-icon dashicons-info">WordPress Support Forums</a></li>
              <li><div class="welcome-icon dashicons-editor-help"><a target="_blank" href="http://www.wpbeginner.com/beginners-guide/how-to-properly-ask-for-wordpress-support-and-get-it/">How to Ask for WordPress Help</a></div></li>
            </ul>
          </div>
          <div class="welcome-panel-column welcome-panel-last">
            <h3>More Help</h3>
            <p style="margin-right:25px;">If you need further assistance, please contact your organization's Web Development or Information Technology department.</p>
          </div>
        </div>
      </div>
    </div>
    <script>
      jQuery(document).ready(function($) {
        $('#dashboard-widgets-wrap').before($('#custom-id').show());
      });
    </script>

<?php }

}


# replace "howdy" in wp admin bar ==============================
if ( ! function_exists( 'jkd__replace_howdy' ) ) {

  add_filter( 'admin_bar_menu', 'jkd__replace_howdy',25 );

  function jkd__replace_howdy( $wp_admin_bar ) {
     $my_account=$wp_admin_bar->get_node('my-account');
     $newtitle = str_replace( 'Howdy,', 'Hello,', $my_account->title );
     $wp_admin_bar->add_node( 
        array(
         'id' => 'my-account',
         'title' => $newtitle,
        ) 
      );
   }

}


# remove WP icon in upper right corner =========================
if ( ! function_exists( 'jkd__admin_bar_remove' ) ) {

  add_action('wp_before_admin_bar_render', 'jkd__admin_bar_remove', 0);

  function jkd__admin_bar_remove() {
    global $wp_admin_bar;
    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');
  }

}


# remove WP version in lower right corner ======================
if ( ! function_exists( 'jkd__footer_shh' ) ) {
  
  add_action( 'admin_menu', 'jkd__footer_shh' );

  function jkd__footer_shh() {
      remove_filter( 'update_footer', 'core_update_footer' ); 
  }

}


# remove "thank you..." lower left corner ======================
if ( ! function_exists( 'jkd__edit_footer' ) ) {

  add_action( 'admin_init', 'jkd__edit_footer' );

  function jkd__edit_footer() {
      add_filter( 'admin_footer_text', 'jkd__edit_text', 11 );
  }

  function jkd__edit_text($content) {
      return " ";
  }

}