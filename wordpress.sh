#!/bin/bash
# WordPress automatic installation & set-up with WP-CLI
# modded from https://premium.wpmudev.org/blog/set-up-wordpress-like-a-pro/

# usage prompt =================================================
if [ $# -lt 3 ]; then
	echo "usage: <db-name> <site-url> <admin-email> <install-dir>"
	exit 1
fi


# setup ====================================================
TITLE=WordPress

# admin
ADMINUSER=jkdeveloper
ADMINPASS=(`cat /dev/random | LC_CTYPE=C tr -dc "[:alnum:]" | head -c 30`)
ADMINEMAIL=$3

# database
DBNAME=$1
DBUSER=homestead
DBPASS=secret
DBHOST=127.0.0.1:33060

# locations
URL=$2
INSTALLDIR=$4

# directory where the script lives so we can copy the mu-plugins later
# needs to be above where we change the directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# generate random string for database prefix
DBGEN=(`cat /dev/random | LC_CTYPE=C tr -dc "[:lower:]" | head -c 5`)
UNDERSCORE='_'
DBPREFIX=$DBGEN$UNDERSCORE



# ==============================================================
# STUFF STARTS HAPPENING!
# ==============================================================
# make and add admin login information to a text file on local desktop
echo "${URL} / ${ADMINUSER} / ${ADMINPASS}" > ~/Desktop/login."$(date +"%Y%m%d_%H%M%S")".txt

# make and change to chosen install directory
mkdir $4
cd $4

# add WordPress-specific .gitignore
echo "# -----------------------------------------------------------------
# .gitignore for WordPress
# https://gist.github.com/indieforger/d915d8b430fef748f966
# -----------------------------------------------------------------
www/readme.html
www/wp-config.php

# ignore uploads
www/wp-content/uploads/*

# ignore WordFence stuff
www/wp-content/plugins/wordfence/tmp/configCache.php
www/wp-content/wflogs/config.php

# ignore Sucuri stuff
www/sucuri/sucuri-auditqueue.php
www/sucuri/sucuri-lastlogins.php

# ignore Updraft stuff
www/wp-content/plugins/updraft/

# track this file .gitignore (i.e. do NOT ignore it)
!.gitignore

# track .editorconfig file (i.e. do NOT ignore it)
!.editorconfig

# track readme.md in the root (i.e. do NOT ignore it)
!readme.md

# ignore OS generated files
ehthumbs.db
Thumbs.db
.DS_Store

# ignore Editor files
*.sublime-project
*.sublime-workspace
*.komodoproject

# ignore log files and databases
*.log
*.sql
*.sqlite
*.txt
readme.txt

# ignore compiled files
*.com
*.class
*.dll
*.exe
*.o
*.so

# ignore packaged files
*.7z
*.dmg
*.gz
*.iso
*.jar
*.rar
*.tar
*.zip

# ignore everything in the wp-content directory, except:
# mu-plugins directory
# plugins directory
# themes directory
wp-content/*
!wp-content/mu-plugins/
!wp-content/plugins/
!wp-content/themes/

# ignore node/grunt dependency directories
node_modules/" > .gitignore

# make & change to www directory within chosen install directory
mkdir www
cd www

# make robots.txt file so WordPress's default/virtual one isn't used
echo "User-agent: *
Disallow:" > robots.txt



# install WordPress ============================================
wp core download;

# probably can remove --skip-check, once localhost vs 127.0.0.1 is sorted
# Includes some extra standard stuff for the wp-config.php file
wp core config --dbname=${DBNAME} --dbuser=${DBUSER} --dbpass=${DBPASS} --dbhost=${DBHOST} --dbprefix=${DBPREFIX} --skip-check --extra-php <<PHP
# CUSTOM SETTINGS =======================================
define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 10 );
define( 'FORCE_SSL_ADMIN', true );
define( 'WP_MEMORY_LIMIT', '96M' );

# FILE EDITS IN WP-ADMIN =======================================
# DISALLOW_FILE_EDIT is for development, we may still need to update plugins
define( 'DISALLOW_FILE_EDIT', true );
# DISALLOW_FILE_MODS is for production, no one should be updating on production
// define( 'DISALLOW_FILE_MODS', true );

# DISABLE AUTO-UPDATE
define( 'AUTOMATIC_UPDATER_DISABLED', true );

## DEBUG =======================================
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );
PHP

wp db create
wp core install --url=${URL} --title=${TITLE} --admin_user=${ADMINUSER} --admin_password=${ADMINPASS} --admin_email=${ADMINEMAIL}



# PLUGINS ======================================================
# instantly activate these plugins -- useful for development
INSTANT_ACTIVATE="monster-widget rtl-tester p3-profiler broken-link-checker user-switching wordpress-importer user-role-editor query-monitor wp-max-submit-protect"

# install these plugins but leave them inactive -- useful for client
INACTIVE="imsanity ewww-image-optimizer all-in-one-seo-pack updraftplus wordfence bwp-minify fastest-cache wps-hide-login"

wp plugin install ${INSTANT_ACTIVATE} --activate --quiet
wp plugin install ${INACTIVE} --quiet

# MU-PLUGINS ===================================================
# It will yell at you that mu-plugins already exists. That's 
# because we installed p3 and that creates mu-plugins for you. But 
# leave this here just in case we remove P3.

mkdir wp-content/mu-plugins


# Get the three mu-plugins bundled with this script

cp "$DIR"/wordpress-cleaner.php ${4}/www/wp-content/mu-plugins
cp "$DIR"/wordpress-admin-cleaner.php ${4}/www/wp-content/mu-plugins
cp "$DIR"/wordpress-shortcodes.php ${4}/www/wp-content/mu-plugins


# Get WordPress plugin boilerplate -- we put as a mu-plugin because often
# it's really important for custom plugins to always run. Installing as a 
# mu-plugin prevents the user from disabling the plugin.

read -p "Do you want to install the WordPress plugin boilerplate? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	git clone https://github.com/DevinVinson/WordPress-Plugin-Boilerplate.git
	cp -R WordPress-Plugin-Boilerplate/plugin-name wp-content/mu-plugins
	rm -rf WordPress-Plugin-Boilerplate
	# Create a file so that the plugin boilerplate can be used as a mu-plugin
	# by default it's structured like a regular plugin -- needs .php file
	# in the mu-plugins directory to work

	echo "<?php

	/**
	 * @wordpress-plugin
	 * Plugin Name:       WordPress Plugin Boilerplate
	 * Plugin URI:        http://google.com
	 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
	 * Version:           1.0.0
	 * Author:            JKD
	 * Author URI:        http://jkdesign.com/
	 * License:           GPL-2.0+
	 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
	 * Text Domain:       plugin-name
	 */

	require_once('plugin-name/plugin-name.php');

	?>" > wp-content/mu-plugins/plugin-name.php
fi


# DUMMY DATA ===================================================
# Official WordPress dummy/theme testing data -- has everything

read -p "Do you want to install the WordPress theme test data? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	curl -O "https://wpcom-themes.svn.automattic.com/demo/theme-unit-test-data.xml"
	wp import theme-unit-test-data.xml --authors=skip --quiet
	rm theme-unit-test-data.xml
fi


# HEISENBERG ==================================================
# https://github.com/JoeCianflone/heisenberg-toolkit

read -p "Do you want to install Heisenberg? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	
	# some funkiness to merge .gitignore files required
	# for some reason heis always eats the .gitignore file
	cd ../
	mv .gitignore _.gitignore
	walt install src www/wp-content/themes/_s/assets
	cat _.gitignore >> .gitignore
	rm _.gitignore
	yarn install
	cd www

	# SASS ========================================================
	# We do not download the WordPress _s/Underscores theme with Sass.
	# However there are some useful bits from its Sass I was copying from
	# Underscores before deleting its Sass/CSS directory each time.
	cp "$DIR"/_wordpress-base.scss ${4}src/sass/modules
	echo "@import 'modules/_wordpress-base.scss';" >> ${4}src/sass/application.scss
	
	gulp

fi


# THEMES =======================================================

# Get _s theme with Sass, activate it
wp scaffold _s _s --theme_name="Theme" --author="JKD" --author_uri="http://jkdesign.com" --activate --sassify

read -p "Do you want to remove _s/Underscores' Sass? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	
	rm -r ${4}/www/wp-content/themes/_s/sass
	rm ${4}/www/wp-content/themes/_s/style.css
	cp "$DIR"/style.css ${4}www/wp-content/themes/_s/

fi

# Delete standard WP themes -- really should be OK with just those
# past twentytwelve, but just in case, also list the older ones

wp theme delete twentyten
wp theme delete twentyeleven
wp theme delete twentytwelve
wp theme delete twentythirteen
wp theme delete twentyfourteen
wp theme delete twentyfifteen
wp theme delete twentysixteen
wp theme delete twentyseventeen

# CLEANUP ======================================================

# We only needed wp importer for theme-unit-test-data.xml
wp plugin deactivate "wordpress-importer"
wp plugin delete "wordpress-importer"

# Get rid of all the default junk
wp post delete 1
wp plugin delete hello.php

# Disallow pingbacks, trackbacks, comments
wp option update default_ping_status closed
wp option update default_comment_status closed
wp option update default_pingback_flag null

# Set pretty permalinks
wp rewrite structure "/%postname%/"
wp rewrite flush


# USERS ========================================================
# custom client role
wp role create clientadmin "Client Admin" --clone="administrator"
wp user create clientadmin client@site.dev --role="clientadmin" --user_pass="apple"

# standard wordpress roles
wp user create subscriber subscriber@site.dev --role="subscriber" --user_pass="apple"
wp user create contributor contributor@site.dev --role="contributor" --user_pass="apple"
wp user create author author@site.dev --role="author" --user_pass="apple"
wp user create editor editor@site.dev --role="editor" --user_pass="apple"


# PAGES ========================================================

# Generate some posts that show up on pretty much every site ever
wp post create --import_id=2000 --post_type=page --post_title="About" --post_status=publish --author=6
wp post create --import_id=2001 --post_type=page --post_title="Contact" --post_status=publish --author=6
wp post create --import_id=2002 --post_type=page --post_title="Privacy Policy" --post_status=publish --author=6
wp post create --import_id=2003 --post_type=page --post_title="Sitemap" --post_status=publish --author=6
wp post create --import_id=2004 --post_type=page --post_title="Home" --post_status=publish --author=6
wp post create --import_id=2005 --post_type=page --post_title="Blog" --post_status=publish --author=6


# OPTIONS ======================================================

# Set timezone to -5 GMT (Eastern Standard Time)
wp option update gmt_offset -5

# Set front page optionss
wp option update show_on_front page
wp option update page_on_front 2004
wp option update page_for_posts 2005

# FIX ==========================================================
# replace 127.0.0.1 with localhost in wp-config.php to make it 
# work. I am 100% sure there's a better way to do this? or a 
# reason why localhost/127.0.0.1 are being funky for me.

sed -i "" -- "s/define('DB_HOST', '127.0.0.1:33060');/define('DB_HOST', 'localhost:33060');/g" wp-config.php

# a happy success message! prints some stuff you'll need...
echo "SUCCESS! You can log in: $URL --- $ADMINUSER --- $ADMINPASS"