<?php

/**
 * @wordpress-cleaner
 * Plugin Name:       WordPress Cleaner
 * Plugin URI:        http://google.com
 * Description:       Provides some essential clean-up of WordPress: removal of emojis, removal of admin bar, removal of script and style versions, and removal of the WordPress version.
 * Version:           1.0.0
 * Author:            JKD
 * Author URI:        http://jkdesign.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-cleaner
 */

if ( ! function_exists( 'jkd_scripts' ) ) {
  /**
   * Enqueue scripts and styles.
   */
  function jkd_scripts() {

    // sometimes wordpress ignores jquery on staging?
    // may need to remove this later
    wp_enqueue_script('jquery');

    // heisenberg compiled stylesheet
    wp_enqueue_style( 'heisenberg-style', get_stylesheet_directory_uri() . '/assets/css/app.css');

    // heisenberg compiled js
    wp_register_script( 'heisenberg-js', get_stylesheet_directory_uri() . '/assets/js/all.js', array(), '20170324', true );
    wp_enqueue_script( 'heisenberg-js' );
    
  }

  add_action( 'wp_enqueue_scripts', 'jkd_scripts' );
}

/**
* enable shortcodes in widgets
*/
add_filter('widget_text', 'do_shortcode');

/**
* enable SVGs in media library
* https://css-tricks.com/snippets/wordpress/allow-svg-through-wordpress-media-uploader/
*/

if ( ! function_exists( 'jkd__mime_types' ) ) {
  function jkd__mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'jkd__mime_types');

}

/**
* disable emojis
* http://wordpress.stackexchange.com/a/185578
*/

if ( ! function_exists( 'jkd__disable_wp_emojicons' ) ) {

  add_action( 'init', 'jkd__disable_wp_emojicons' );

  function jkd__disable_wp_emojicons() {

    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'jkd__disable_emojicons_tinymce' );
  }

}


if ( ! function_exists( 'jkd__disable_emojicons_tinymce' ) ) {

  function jkd__disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
      return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
      return array();
    }
  }

}


/**
* strip script and style versions
*/
if ( ! function_exists( 'jkd__remove_enqueue_version' ) ) {

  add_filter( 'style_loader_src', 'jkd__remove_enqueue_version' );
  add_filter( 'script_loader_src', 'jkd__remove_enqueue_version' );

  function jkd__remove_enqueue_version( $url )
  {
      return remove_query_arg( 'ver', $url );
  }

}


/**
* strip wordpress version
*/
if ( ! function_exists( 'jkd__remove_version' ) ) {
  
  add_filter('the_generator', 'jkd__remove_version');

  function jkd__remove_version() {
    return '';
  }

}

/**
* Remove RSD and WLW links from header
*/
remove_action ('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');


/**
* strip admin bar from front end
*/
if ( ! function_exists( 'jkd__remove_admin_bar' ) ) {

  add_action('after_setup_theme', 'jkd__remove_admin_bar');

    function jkd__remove_admin_bar() {
      if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
        show_admin_bar(false);
      }
    }

}